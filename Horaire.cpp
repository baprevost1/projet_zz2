#include "Horaire.hpp"

Horaire::Horaire(const char* j, double h) : jour(j), heure(h), nbPatient(0), nbRessource(0),nbEtape(0){
    patient = new Patient[100];
    ressource = new Ressource[100];
    etape = new Etape[100];   
}
Horaire::Horaire() : jour(""), heure(0.0), nbPatient(0), nbRessource(0),nbEtape(0){
    patient = new Patient[100];
    ressource = new Ressource[100]; 
    etape = new Etape[100];
} 
void Horaire::ajouterPatient(Patient p){
    patient[nbPatient]=p;     
    nbPatient++;
} 
void Horaire::ajouterRessource(Ressource r){
    ressource[nbRessource]=r; 
    nbRessource++;
} 
void Horaire::ajouterEtape(Etape e){

    etape[nbEtape]=e;
    /*etape[nbEtape].setDuree(e.getDuree());
    etape[nbEtape].setNom(e.getNom());
    etape[nbEtape].setEtat(e.getEtat());*/
    for(int i = 0; i< e.getNbressource(); i++){
        for (int j = 0; j< nbRessource; j++){
            if (ressource[j].getNom() == e.getRessource()[i].getNom() && ressource[j].getEtat() == true  ){
                //etape[nbEtape].ajouter(e.getRessource()[i]);
                ressource[j].setEtat(false);
                break; 
            } 
        } 
    } 
    nbEtape++; 
} 
void Horaire::setJour(const char* j){
    jour=j;
} 
void Horaire::setHeure(double h){
    heure = h;
}
void Horaire::setNbressource(int n){
    nbRessource=n;
} 
int Horaire::getNbpatient(){
    return nbPatient;
} 
std::string Horaire::getJour(){
    return jour;
} 
double Horaire::getHeure(){
    return heure;
} 
Ressource* Horaire::getRessource(){
    return ressource;
} 
int Horaire::getNbressource(){
    return nbRessource;
}  
int Horaire::getNbetape(){
    return nbEtape;
}  
bool Horaire::libre(Ressource r){
    bool l = false;
    for (int i = 0; i<nbRessource; i++){
        if(ressource[i].getNom()==r.getNom()){
            if(ressource[i].getEtat() == true ){
                l = true;
                break;
            } 
        } 
    } 
    return l;
} 
int Horaire::getNbPatient(){
    return nbPatient;
} 
Etape* Horaire::getEtape(){
    return etape;
} 
Patient* Horaire::getPatient(){
    return patient;
}
void Horaire::retirerPatient(int IdPatient){
    Patient pp;
    int position = -1;
    for (int i = 0; i < nbPatient; i++)
    {
        if(patient[i].getId() == IdPatient)
        { 
            position = i;
            break;
        } 
    }

    if(position!=-1){
        retirerEtape(position);
        for (int i= position; i < nbPatient-1; i++ ){
            //patient[i].setId(patient[i+1].getId()); 
            //patient[i].setMaladie(patient[i+1].getMaladie()); 
            patient[i]=patient[i+1]; 
        }
        patient[nbPatient]=pp; 
        nbPatient--;
    } 

} 
void Horaire::retirerEtape(int position){
    Etape ee;
    etape[position].setEtat(false); 
    retirerRessource(etape[position].getRessource(),etape[position].getNbressource());
    for (int i= position; i < nbEtape-1; i++ ){
        etape[i] = etape[i+1];
    }
    etape[nbEtape]=ee; 
    nbEtape--;
} 
void Horaire::retirerRessource(Ressource* r, int nbr){
    for(int i=0; i<nbr;i++){
        for(int j = 0; j<nbRessource; j++){
            if (ressource[j].getNom()==r[i].getNom()&&ressource[j].getEtat()==false ){
                ressource[j].setEtat(true);
                break;
            } 
        } 
    } 
} 
void Horaire::setNbetape(int n){
    nbEtape=n;
}
void Horaire::setNbPatient(int n){
    nbPatient=n;
}    
/*Horaire::~Horaire(){
    delete[] patient;
    delete[] etape;
    delete[] ressource;
} */