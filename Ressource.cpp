#include "Ressource.hpp"

Ressource::Ressource(const char* name) : nom(name),etat(true){} 
Ressource::Ressource() : nom(""),etat(false){} 
std::string Ressource::getNom(){
    return nom;
} 
void Ressource::setNom(std::string name){
    nom=name;
}  
bool Ressource::getEtat(){
    return etat;
} 
void Ressource::setEtat(bool e){
    etat = e;
} 
//Ressource::~Ressource(){}  