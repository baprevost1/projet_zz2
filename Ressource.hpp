#ifndef _RESSOURCE_
#define _RESSOURCE_

#include <iostream>
#include <string> 
#include <cstring>

class Ressource{
    private:
        std::string nom;
        bool etat;
    public:
        Ressource(const char* name);
        Ressource();
        //~Ressource();
        std::string getNom();
        void setNom(std::string name);
        bool getEtat();
        void setEtat(bool e);
};
#endif 