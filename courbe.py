import csv
import matplotlib.pyplot as plt
import numpy as np

def plot_data_from_csv(csv_file):
    # Lecture des données à partir du fichier CSV
    with open(csv_file, 'r') as file:
        reader = csv.reader(file)
        # Ignorer l'en-tête s'il existe
        next(reader)
        # Initialiser des listes pour stocker les données
        x_values = []
        y_values_1 = []
        y_values_2 = []
        # Parcourir les lignes du fichier CSV
        for row in reader:
            # Convertir les valeurs en flottants
            x_values.append(float(row[0]))
            y_values_1.append(float(row[1]))
            y_values_2.append(float(row[2]))

    # Trier les valeurs x et conserver l'ordre pour les valeurs y
    sorted_indices = sorted(range(len(x_values)), key=lambda k: x_values[k])
    x_values = [x_values[i] for i in sorted_indices]
    y_values_1 = [y_values_1[i] for i in sorted_indices]
    y_values_2 = [y_values_2[i] for i in sorted_indices]
    
    # Créer le graphique à partir des données
    if (csv_file=='score_LS.csv'):
        plt.plot(x_values, np.array(y_values_1)-np.array(y_values_2), label='Local Search = 10', color='blue')
        plt.title('Difference de score entre greedy et Local Search')
        plt.xlabel('Nombre de patients')
        plt.ylabel('Difference de score')
        plt.legend()
        plt.grid(True)
        plt.show()
    else :
        plt.plot(x_values, y_values_1, label='Algorithme greedy', color='blue')
        plt.plot(x_values, y_values_2, label='Algorithme greedy2', color='red')
        plt.title('Comparaison des scores')
        plt.xlabel('Nombre de patients')
        plt.ylabel('Score du calendrier')
        plt.legend()
        plt.grid(True)
        plt.show()

# Appeler la fonction avec le nom du fichier CSV
plot_data_from_csv('score_LS.csv')
