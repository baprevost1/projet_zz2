#ifndef _HORAIRE_
#define _HORAIRE_

#include <string>
#include <iostream> 
#include <fstream>
#include <cstring>

#include "Patient.hpp" 
#include "Ressource.hpp" 
#include "Etape.hpp" 

class Horaire {
    private:
        Patient* patient;
        int nbPatient;
        Etape* etape;
        int nbEtape;
        Ressource* ressource;
        int nbRessource;
        std::string jour; 
        double heure;
    public:
        Horaire(const char* j, double h);
        Horaire();
        //~Horaire();
        void setJour(const char* j);
        void setHeure(double h);
        void setNbressource(int n);
        std::string getJour();
        double getHeure();
        int getNbpatient();
        void setNbPatient(int n);
        Ressource* getRessource();
        int getNbressource();
        bool libre(Ressource r);
        int getNbPatient();
        Patient* getPatient();
        Etape* getEtape();
        int getNbetape();
        void setNbetape(int n);
        //void setEtape(Etape *e);
        void ajouterPatient(Patient p);
        void ajouterRessource(Ressource r);
        void ajouterEtape(Etape e);
        void retirerPatient(int IdPatient);
        void retirerEtape(int position);
        void retirerRessource(Ressource* r,int nbr);
};


#endif

