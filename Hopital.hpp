#ifndef _HOPITAL_
#define _HOPITAL_

#include <iostream>
#include <string> 

#include "Ressource.hpp" 

class Hopital{
    private:
        std::string nom;
        Ressource *ressource; 
        int nbressource;
        int nbDocteurPartiel;
        int nbDocteur;
        int nbMateriel;
    public:
        Hopital(const char* name);
        Hopital();
        //~Hopital();
        std::string getNom();
        Ressource *getRessource();
        int getNbressource();
        void ajouter(Ressource r);
        void ajouterPartiel(Ressource r);
        int getNbdocteur();
        int getNbmateriel();
        void afficher();
};
#endif 