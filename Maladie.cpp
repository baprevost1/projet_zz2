#include "Maladie.hpp"

Maladie::Maladie(const char* name) : nom(name),nbetape(0){
    etape = new Etape[100];
    //FAIS UN SETNOM ET COPIE TOUT 
    if(nom=="Fracture"){
        nbetape = 3;
        Ressource r("Docteur");
        Ressource r2("Materiel");

        etape[0].setNom("Radio");
        etape[0].setDuree(3);
        etape[0].ajouter(r); 
        etape[0].ajouter(r2);

        etape[1].setNom("Operation"); 
        etape[1].setDuree(6);
        etape[1].ajouter(r);
        etape[1].ajouter(r);

        etape[2].setNom("Bequille"); 
        etape[2].setDuree(1);
        etape[2].ajouter(r2);
    } 
    if(nom=="Covid"){
        nbetape = 1;

        Ressource r("Docteur");
        etape[0].setNom("Vaccin"); 
        etape[0].setDuree(2);
        etape[0].ajouter(r);
    } 
    if (nom == "Cancer"){
        nbetape = 4;

        Ressource r("Docteur");
        Ressource r2("Materiel");
        for (int i=0;i<nbetape;i++){
            etape[i].setNom("Chimiotherapie"); 
            etape[i].setDuree(6);
            etape[i].ajouter(r);
            etape[i].ajouter(r2);
            etape[i].ajouter(r2);
        } 
    } 
    if (nom == "Accouchement"){
        nbetape=2;
        Ressource r("Docteur");
        Ressource r2("Materiel");

        etape[0].setNom("Echographie");
        etape[0].setDuree(1);
        etape[0].ajouter(r);
        etape[0].ajouter(r2);

        etape[1].setNom("Mis au monde");
        etape[1].setDuree(8);
        etape[1].ajouter(r);
    }  
}  
Maladie::Maladie() : nom(""),nbetape(0){
    etape = new Etape[100];
}  
Etape* Maladie::getEtape(){
    return etape;
} 
void Maladie::ajouter(Etape e){
    etape[nbetape]=e; 
    nbetape++;
} 
std::string Maladie::getNom(){
    return nom;
} 
int Maladie::getNbetape(){
    return nbetape;
} 
void Maladie::setNom(const char* name){
    nom=name;
    if(nom=="Fracture"){
        nbetape = 3;
        Ressource r("Docteur");
        Ressource r2("Materiel");

        etape[0].setNom("Radio");
        etape[0].setDuree(3);
        etape[0].ajouter(r); 
        etape[0].ajouter(r2);

        etape[1].setNom("Operation"); 
        etape[1].setDuree(6);
        etape[1].ajouter(r);
        etape[1].ajouter(r);

        etape[2].setNom("Bequille"); 
        etape[2].setDuree(1);
        etape[2].ajouter(r2);
    } 
    if(nom=="Covid"){
        nbetape = 1;

        Ressource r("Docteur");
        etape[0].setNom("Vaccin"); 
        etape[0].setDuree(2);
        etape[0].ajouter(r);
    } 
    if (nom == "Cancer"){
        nbetape = 4;

        Ressource r("Docteur");
        Ressource r2("Materiel");
        for (int i=0;i<nbetape;i++){
            etape[i].setNom("Chimiotherapie"); 
            etape[i].setDuree(6);
            etape[i].ajouter(r);
            etape[i].ajouter(r2);
            etape[i].ajouter(r2);
        } 
    } 
    if (nom == "Accouchement"){
        nbetape=2;
        Ressource r("Docteur");
        Ressource r2("Materiel");

        etape[0].setNom("Echographie");
        etape[0].setDuree(1);
        etape[0].ajouter(r);
        etape[0].ajouter(r2);

        etape[1].setNom("Mis au monde");
        etape[1].setDuree(8);
        etape[1].ajouter(r);
    }  
} 
/*Maladie::~Maladie() {
    delete[] etape;
} */