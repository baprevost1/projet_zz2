#include "Population.hpp"

Population::Population(){
    // Initialisation du générateur de nombres aléatoires avec une seed basée sur l'horloge actuelle
    std::random_device rd;
    std::mt19937 gen(rd());

    // Définir la distribution pour générer des nombres entre 1 et 100 inclusivement
    std::uniform_int_distribution<int> distribution(1, 100);

    // Générer un nombre aléatoire
    int r = distribution(gen);
    if(r<50){
        r+=50;
    } 

    nbPatient = r;

    patient = new Patient[nbPatient]; 
    for (int i=0; i<nbPatient;i++){
        int r2 = distribution(gen);
        if(r2<50){
            Maladie m("Fracture");
            patient[i].setMaladie(m);
        } 
        else if(r2<90&&r2>49){
            Maladie m("Covid");
            patient[i].setMaladie(m);
        } 
        if(r2<100&&r2>89){
            Maladie m("Accouchement");
            patient[i].setMaladie(m);
        } 
        if(r2==100){
            Maladie m("Cancer");
            patient[i].setMaladie(m);
        } 
    } 
} 
int Population::getNbPatient(){
    return nbPatient;
} 
void Population::afficher(){
    std::cout << "Il y a "<< nbPatient<< " patient."<<std::endl;
    for (int i=0; i<nbPatient; i++){
        std::cout << "Le patient "<< i <<" a "<< patient[i].getMaladie().getNom()<< std::endl;
    } 
} 
Patient* Population::getPatient(){
    return patient;
} 

void Population::RAZ(){
    for(int i=0;i<nbPatient;i++){
        for (int j=0;j<patient[i].getMaladie().getNbetape();j++){
            patient[i].getMaladie();

        } 
    } 
} 

/*Population::~Population(){
    delete[] patient;
}  */