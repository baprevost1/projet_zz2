#ifndef _PATIENT_
#define _PATIENT_

#include <iostream>
#include <string> 

#include "Maladie.hpp"

class Patient {
    private:
        static int cle;
        int id;
        Maladie maladie;
        bool guerrie;
        double dureeHopital; // Temps que le patient reste dans l'hopital en minutes

    public:
        Patient(Maladie m); 
        Patient();
        //~Patient();
        int getId();
        Maladie getMaladie();
        void setMaladie(Maladie m);
        void setId(int i);
        bool getGuerrie();
        double getDureeHopital();
        void ajouterDureeHopital(double dureeH);
        void setDureehopital(double d);
        static int getCle();
};
#endif
