#ifndef _ETAPE_
#define _ETAPE_

#include <iostream>
#include <string> 
#include <cstring>

#include "Ressource.hpp" 

class Etape{
    private:
        std::string nom;
        Ressource *ressource; 
        int nbressource;
        int duree;
        bool etat;
    public:
        Etape(const char* name, int temps);
        Etape();
        //~Etape();
        Ressource *getRessource();
        void ajouter(Ressource r);
        int getDuree();
        void setDuree(int temps);
        std::string getNom();
        bool getEtat();
        int getNbressource();
        void setEtat(bool e);
        void setNom(const char* name);
        void setNom(std::string name);
};
#endif 