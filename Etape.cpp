#include "Etape.hpp"

Etape::Etape(const char* name, int temps) : nom(name),nbressource(0), duree(temps), etat(false){
    ressource = new Ressource[100]; 
}  
Etape::Etape() : nom(""),nbressource(0), duree(0),etat(false) {
    ressource = new Ressource[100]; 
} 
Ressource* Etape::getRessource(){
    return ressource;
} 
void Etape::ajouter(Ressource r){
    ressource[nbressource]=r; 
    nbressource++;
} 
int Etape::getDuree(){
    return duree;
}   
std::string Etape::getNom(){
    return nom;
} 
bool Etape::getEtat(){
    return etat;
} 
int Etape::getNbressource(){
    return nbressource;
} 
void Etape::setEtat(bool e){
    etat = e;
} 
void Etape::setNom(const char* name){
    nom=name;
} 
void Etape::setNom(std::string name){
    nom=name;
} 
void Etape::setDuree(int temps){
    duree=temps;
} 
/*Etape::~Etape(){
    delet[] reressource;
}*/