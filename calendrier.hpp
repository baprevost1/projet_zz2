#ifndef _CALENDRIER_HPP
#define _CALENDRIER_HPP

#include <string>
#include <iostream> 
#include <fstream>
#include <cstring>
#include <iomanip>
#include <cmath>
#include <set>
#include <random>

#include "Patient.hpp" 
#include "Hopital.hpp" 
#include "Horaire.hpp" 

class Calendrier{
    private:
        Hopital hopital;
        int semaine;
        int nbPatient;
        Patient* patient;
        Horaire* horaire;
    public:
        Calendrier(Hopital chu);
        Calendrier();
        //~Calendrier();

        void CalendrierToCSV1() const; // creer un fichier .csv
        void CalendrierToCSV2() const;
        void ajouter(Patient p);
        Hopital getHopital();
        Horaire *getHoraire();
        Patient *getPatient();
        void setHoraire(Horaire* h);
        //void setHoraire(horaire h);
        void setHopital(Hopital hop);
        int getNbPatient();
        void afficher();
        void greedy();
        void greedy2();
        void tempsSejour(); // Calcul le temps que reste un patient dans l'hopital
        void tempsSejour(int nbp);
        double score();
        double score(int nbp);
        void Local_Search(int iterMax);
        void DeplacerPatient(int IdPatient, int numEtape, int oldHoraire, int newHoraire);
        void regrets(int k);
        int NbAleat(int deb, int fin);
        bool InsertionPatientPossible(int IdPatient, int etape, int deb, int fin, int& position); //On vérifie juste pour une étape
        double scoreUnPatient(int IdPatient, int date[]);
        void RAZ();
        double maxListe(int tab[]);
        double minListe(int tab[]);
        double maxListe(double tab[], int k);
        double minListe(double tab[], int k);
        void RAZ_patient();
        void remove(int id);
        int PremiereApparitionPatient(int id);
        int DerniereApparitionPatient(int id);
        //bool operator == (Calendrier c);
};


#endif

