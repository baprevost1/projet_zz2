#ifndef _MALADIE_
#define _MALADIE_

#include <iostream>
#include <string> 

#include "Ressource.hpp" 
#include "Etape.hpp" 


class Maladie{
    private:
        std::string nom;
        Etape *etape;
        int nbetape;
    public:
        Maladie(const char* name);
        Maladie();
        //~Maladie();
        Etape* getEtape();
        void ajouter(Etape e);             
        std::string getNom();
        int getNbetape();
        void setNom(const char* name);
};
#endif 