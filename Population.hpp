#ifndef _POPULATION_
#define _POPULATION_

#include <string>
#include <iostream> 
#include <fstream>
#include <cstring>
#include <random>

#include "Patient.hpp"  

class Population {
    private:
        Patient* patient;
        int nbPatient;
    public:
        Population();
        //~Population();
        int getNbPatient();
        void afficher();
        Patient* getPatient();
        void RAZ();
};


#endif

