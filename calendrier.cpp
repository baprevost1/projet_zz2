#include "calendrier.hpp"

Calendrier::Calendrier(Hopital chu) : hopital(chu),nbPatient(0), semaine(0){
    patient = new Patient[100]; 
    horaire = new Horaire[155];
    double h=8.0;
    int j=0;
    int l=0;
    for (int i = 0; i<155; i++){
        if (i%31 < 13){
            //std::cout << "Horaire " << i << " sans les Partiel" << std::endl;
            int j=0;
            int l=0;
            //std::cout << "nb de doc " <<hopital.getNbdocteur()<< std::endl;
            while (l<hopital.getNbdocteur()){
                if (hopital.getRessource()[j].getNom()=="Docteur")
                {
                    horaire[i].ajouterRessource(hopital.getRessource()[j]);
                    l++;
                } 
                j++;
            } 
            j=0;
            l=0;
            while (l<hopital.getNbmateriel()){
                if (hopital.getRessource()[j].getNom()=="Materiel")
                {
                    horaire[i].ajouterRessource(hopital.getRessource()[j]);
                    l++;
                } 
                j++;
            } 
        } 
        else{
            //std::cout << "Horaire " << i << " avec les Partiel" << std::endl;
            for (int j=0; j< hopital.getNbressource(); j++){
                horaire[i].ajouterRessource(hopital.getRessource()[j]); 
            } 
        } 
        if(i%31==0){
            j++;
            h=8.0;
            l=0;
        } 
        if (j==1){
            horaire[i].setJour("Lundi");
        }
        if (j==2){
            horaire[i].setJour("Mardi");
        }  
        if (j==3){
            horaire[i].setJour("Mercredi");
        } 
        if (j==4){
            horaire[i].setJour("Jeudi");
        } 
        if (j==5){
            horaire[i].setJour("Vendredi");
        } 
        if(l%3==0){
            horaire[i].setHeure(h);
        } 
        if(l%3==1){
            horaire[i].setHeure(h+0.2);
        } 
        if(l%3==2){
            horaire[i].setHeure(h+0.4);
            h=h+1;
        }
        l++;
    } 
}
Calendrier::Calendrier() : hopital(),nbPatient(0), semaine(0){
    patient = new Patient[100]; 
    horaire = new Horaire[155];
    } 
void Calendrier::setHopital(Hopital hop){
    hopital=hop;
    double h=8.0;
    int j=0;
    int l=0;
    for (int i = 0; i<155; i++){
        if (i%31 < 13){
            //std::cout << "Horaire " << i << " sans les Partiel" << std::endl;
            int j=0;
            int l=0;
            //std::cout << "nb de doc " <<hopital.getNbdocteur()<< std::endl;
            while (l<hopital.getNbdocteur()){
                if (hopital.getRessource()[j].getNom()=="Docteur")
                {
                    horaire[i].ajouterRessource(hopital.getRessource()[l]);
                    l++;
                } 
                j++;
            } 
            j=0;
            l=0;
            while (l<hopital.getNbmateriel()){
                if (hopital.getRessource()[j].getNom()=="Materiel")
                {
                    horaire[i].ajouterRessource(hopital.getRessource()[l]);
                    l++;
                } 
                j++;
            } 
        } 
        else{
            //std::cout << "Horaire " << i << " avec les Partiel" << std::endl;
            for (int j=0; j< hopital.getNbressource(); j++){
                horaire[i].ajouterRessource(hopital.getRessource()[j]); 
            } 
        } 
        if(i%31==0){
            j++;
            h=8.0;
            l=0;
        } 
        if (j==1){
            horaire[i].setJour("Lundi");
        }
        if (j==2){
            horaire[i].setJour("Mardi");
        }  
        if (j==3){
            horaire[i].setJour("Mercredi");
        } 
        if (j==4){
            horaire[i].setJour("Jeudi");
        } 
        if (j==5){
            horaire[i].setJour("Vendredi");
        } 
        if(l%3==0){
            horaire[i].setHeure(h);
        } 
        if(l%3==1){
            horaire[i].setHeure(h+0.2);
        } 
        if(l%3==2){
            horaire[i].setHeure(h+0.4);
            h=h+1;
        }
        l++;
    } 
} 

/*Calendrier::~Calendrier(){
    delete[] patient;
    delete[] horaire;
}    */

int Calendrier::getNbPatient(){
    return nbPatient;
} 
Patient* Calendrier::getPatient(){
    return patient;
} 
void Calendrier::CalendrierToCSV1() const {
    std::ofstream FichierSortie("planning1.csv");
    if (!FichierSortie) {
        std::cerr << "Erreur lors de l'ouverture du fichier." << std::endl;
        return;
    }
    FichierSortie << std::fixed << std::setprecision(2);

    FichierSortie << "Horaire/Jour," << "lundi,mardi,mercredi,jeudi,vendredi" << std::endl;
    //FichierSortie << "pb ici?,";
    for (int i = 0; i < 31; i++) { // boucle pour les heures
        FichierSortie << horaire[i].getHeure() ;

        // On cherche à déterminer le nombre maximum de patients que l'on peut avoir sur un horaire pour avoir un affichage propre
        int max_patient = 0;
        for (int j = 0; j < 5; j++){ // boucle pour déterminer le max de patient qu'on peut avoir à un horaire
            int heure = j*31 + i;
            if (horaire[heure].getNbPatient() > max_patient)
                max_patient = horaire[heure].getNbPatient();
        } 
        //std::cout << "max patients : " << max_patient << std::endl;
        for (int j = 0; j < max_patient; j++){ // boucle pour afficher tous les patients associés à 1 horaire
            int iter = 0;
            for (int k = 0; k < 5; k++){  // boucle pour les jours
                int e = k*31 + i;
                int nbPatient = horaire[e].getNbPatient();
                if (iter < nbPatient && horaire[e].getPatient()[j].getId()<100) //deuxième condition car sinon il affiche des patients qui n'existent pas et je comprnds pas d'où ça vient
                { /*
                    int l=0;
                    while (horaire[e].getPatient()[j].getMaladie().getEtape()[l].getEtat() == true && l < horaire[e].getPatient()[j].getMaladie().getNbetape()){
                        l++;
                    } */
                    FichierSortie << ",Patient " << horaire[e].getPatient()[j].getId() << " pour " << horaire[e].getPatient()[j].getMaladie().getNom() << " effectue " << horaire[e].getEtape()[j].getNom();/*" avec " << horaire[e].getPatient()[k].getMaladie().getEtape()[0].getRessource()[0].getNom();*/
                    //iter ++;
                }
                else
                    FichierSortie << " ,";
            }
            FichierSortie << std::endl; 
        }
        if (max_patient == 0)
            FichierSortie << std::endl;
        FichierSortie << std::endl;
    }
}
void Calendrier::CalendrierToCSV2() const {
    std::ofstream outFic("planning2.csv");
    if (!outFic) {
        std::cerr << "Erreur lors de l'ouverture du fichier." << std::endl;
        return;
    }
    outFic << std::fixed << std::setprecision(2);

    outFic << "Horaire/Jour," << "lundi,mardi,mercredi,jeudi,vendredi" << std::endl;
    for (int i = 0; i < 31; i++) { // boucle pour les heures
        outFic << horaire[i].getHeure() << ",";
        for (int k = 0; k < 5; k++){  // boucle pour les jours
            int heure = k*31 + i;
            for (int j = 0; j < horaire[heure].getNbPatient() ; j++){ // boucle pour afficher tous les patients associés à 1 horaire
                outFic << "Patient " << horaire[heure].getPatient()[j].getId() << " pour " << horaire[heure].getPatient()[j].getMaladie().getNom();
                int l=0;
                while (horaire[heure].getPatient()[j].getMaladie().getEtape()[l].getEtat() == true){
                    l++;
                } 

                outFic << " effectue " << horaire[heure].getPatient()[j].getMaladie().getEtape()[l].getNom();
                outFic << "/";
            }
            outFic << " , ";
        }
        outFic << std::endl; 
    }
}
void Calendrier::ajouter(Patient p){
    patient[nbPatient].setId(p.getId()); 
    patient[nbPatient].setMaladie(p.getMaladie());
    nbPatient++;
} 
Hopital Calendrier::getHopital(){
    return hopital;
}
Horaire *Calendrier::getHoraire(){
    return horaire;
} 
void Calendrier::setHoraire(Horaire* h){ 
    for(int i=0; i<155;i++){
        //std::cout<<"i="<< i << std::endl;
        /*horaire[i].setNbressource(h[i].getNbressource());  
        for(int j=0;j<h[i].getNbressource();j++){
            horaire[i].getRessource()[j].setNom(h[i].getRessource()[j].getNom());
            horaire[i].getRessource()[j].setEtat(h[i].getRessource()[j].getEtat()); 
        } */
        //horaire[i].setNbetape(0); 
        //std::cout<<h[i].getNbetape()<<std::endl;
        //std::cout << "H1" << std::endl; 
        int nbe=h[i].getNbetape();
        for (int j=0;j< nbe;j++){
            horaire[i].ajouterEtape(h[i].getEtape()[j]);  
        } 
        std::cout << "H2" << std::endl;
        //std::cout <<"salut"<<std::endl;
        //horaire[i].setNbPatient(0); 
        //std::cout <<"nbpatient 2nbp="<<h[i].getNbpatient()<<std::endl;
        int nbp=h[i].getNbpatient();
        for (int j=0;j< nbp;j++){
            horaire[i].ajouterPatient(h[i].getPatient()[j]);  
        } 
        std::cout << "H3" << std::endl;
    } 
} 
void Calendrier::afficher(){
    for( int i = 0; i< 155 ; i++){
        if(i%31==0){
            std::cout << std::endl;
            std::cout << horaire[i].getJour() << std::endl; 
        } 
        std::cout << horaire[i].getHeure() << " | ";
        if(horaire[i].getNbpatient()>0){
            for (int j=0; j<horaire[i].getNbpatient(); j++){
                std::cout << "avec le patients " << horaire[i].getPatient()[j].getId();
                std::cout << ". Pour faire "<< horaire[i].getEtape()[j].getNom() << " | "; 
            } 
        } /*
        for (int j=0;j<horaire[i].getNbressource(); j++){
            if (horaire[i].getRessource()[j].getEtat()==true){
                std::cout << "et avec "<<horaire[i].getRessource()[j].getNom() << " | " ;
            } 
        } */
    } 
    std::cout << std::endl;
} 
void Calendrier::greedy(){
    for(int i = 0; i<nbPatient; i++){
        std::cout << "\n\t\t  Patient numero "<<i<<std::endl;
        if (patient[i].getGuerrie()==false){
            Maladie m = patient[i].getMaladie();
            std::cout << "\t Est malade de "<<m.getNom()<<std::endl;
            std::cout << "\t Il faut "<<m.getNbetape()<<" etape pour le soigner"<<std::endl;
            int unew=0;
            for (int j = 0; j<m.getNbetape();j++){
                if(m.getEtape()[j].getEtat()==false){
                    std::cout << "Etape "<<j << " : "<< m.getEtape()[j].getNom()<< " n'est pas encore réalisée.";
                    Etape e = m.getEtape()[j];  
                    std::cout << " Elle dure "<<e.getDuree()<<std::endl;          
                    int faisable;
                    for (int u = unew; u<155; u++){
                        faisable=1;
                        if (u+e.getDuree()>155){
                            break;
                        } 
                        //std::cout << "Duree : "<<e.getDuree() << ". Heure debut : " << horaire[u].getHeure()<< ". Heure fin : "<< horaire[u+e.getDuree()].getHeure()<< std::endl;
                        if (horaire[u+e.getDuree()].getHeure() > horaire[u].getHeure()){
                            for (int b=0;b<horaire[u].getNbressource(); b++){
                                if (horaire[u].getRessource()[b].getEtat()==true){
                                    //std::cout << "L'horaire "<<u<< " a le "<<horaire[i].getRessource()[b].getNom() << " de disponible "<<std::endl;
                                } 
                            } 
                            for (int l=0; l<e.getNbressource(); l++){
                                for (int k=0; k<e.getDuree(); k++){
                                    if(horaire[u+k].libre(e.getRessource()[l])==false){
                                        faisable = 0;
                                        break;
                                    } 
                                } 
                                if(faisable == 0){
                                    break;
                                } 
                            }
                        } 
                        else{
                            faisable = 0;
                        } 
                        if (faisable == 0){
                            //std::cout << "Impossible de la planifié à l'horaire "<< u <<std::endl;
                        } 
                        if (faisable == 1){
                            std::cout << "Elle est planifié à l'horaire "<< u <<std::endl;
                            for (int k = 0; k<e.getDuree(); k++){
                                horaire[u+k].ajouterPatient(patient[i]);
                                horaire[u+k].ajouterEtape(e);
                                horaire[u+k].getPatient()[horaire[u+k].getNbPatient()-1].getMaladie().getEtape()[j].setEtat(true);  
                            } 
                            //std::cout << "C'est une " << e.getNom()<<std::endl;
                            int nbet=horaire[u].getNbetape();
                            std::cout << "C'est une " << horaire[u].getEtape()[nbet-1].getNom()<<std::endl;
                            unew=u+e.getDuree();
                            e.setEtat(true);
                            std::cout << "L'etape "<< j << " est terminer" << ", etat de l'etape : "<< e.getEtat() <<std::endl;
                            break;
                        } 
                    } 
                } 
            } 
        } 
    } 
} 

void Calendrier::greedy2(){
    Calendrier tmp(hopital);

    //tmp.RAZ();
    for (int j=0;j<tmp.getNbPatient();j++){
            tmp.remove(tmp.getPatient()[j].getId());
            //tmp.remove(j);
    }

    for(int i=0;i<nbPatient;i++){
        tmp.ajouter(patient[i]);
    } 

    for (int i=0; i<nbPatient; i++){
        std::cout << "\n\t\t  Patient numero "<<i<<std::endl;
        if (patient[i].getGuerrie()==false){
            Maladie m = patient[i].getMaladie();
            //std::cout << "\t Est malade de "<<m.getNom()<<std::endl;
            //std::cout << "\t Il faut "<<m.getNbetape()<<" etape pour le soigner"<<std::endl;
            int unew=0;
            for (int j = 0; j<m.getNbetape();j++){
                if(m.getEtape()[j].getEtat()==false){
                    for (int g=0;g<tmp.getNbPatient();g++){
                        tmp.remove(tmp.getPatient()[g].getId());
                        //tmp.remove(j);
                    }
                    tmp.setHoraire(horaire);
                    //std::cout << "Etape "<<j << " : "<< m.getEtape()[j].getNom()<< " n'est pas encore réalisée.";
                    Etape e = m.getEtape()[j]; 
                    //std::cout << " Elle dure "<<e.getDuree()<<std::endl; 
                    int faisable;
                    double min_score = (1+106.2)*(1+106.2)*nbPatient;
                    int ubest;
                    for (int u = unew; u<155; u++){
                        faisable=1;
                        //std::cout << "Pour l'horaire "<<u<<std::endl; 
                        //std::cout << "Duree de l'etape "<<e.getDuree()<<std::endl;
                        //std::cout << "erreur 0" << std::endl;
                        if (u+e.getDuree()>155){
                            //std::cout << " PAS ASSEZ DE TEMPS "<<std::endl; 
                            break;
                        }
                        //std::cout << "erreur 1" << std::endl;
                        if (e.getDuree()<31 && horaire[u+e.getDuree()].getHeure() > horaire[u].getHeure()){
                            //std::cout << "erreur 2" << std::endl;
                            for (int l=0; l<e.getNbressource(); l++){
                                for (int k=0; k<e.getDuree(); k++){
                                    if(horaire[u+k].libre(e.getRessource()[l])==false){
                                        faisable = 0;
                                        //std::cout << " PAS ASSEZ DE RESSOURCES "<<std::endl; 
                                        break;
                                    } 
                                } 
                                if(faisable == 0){
                                    break;
                                } 
                            }
                        }
                        else{
                            //std::cout << "erreur 3" << std::endl;
                            faisable = 0;
                            //std::cout << "Impossibilite a l'horaire "<< u <<std::endl;
                        } 
                        if (faisable == 1){
                            //std::cout << "erreur 4" << std::endl;
                            //std::cout << "Faisable à l'horaire "<< u <<std::endl; 
                            for (int k = 0; k<e.getDuree(); k++){
                                tmp.getHoraire()[u+k].ajouterPatient(patient[i]);
                                tmp.getHoraire()[u+k].ajouterEtape(e);
                            }
                            //std::cout << "erreur 5" << std::endl;
                            //std::cout << " Nb de patient a l'horaire "<< u << " " << tmp.getNbPatient() <<std::endl; 
                            //std::cout<< "temps de sejour"<<std::endl;
                            tmp.tempsSejour();
                            //std::cout << "Le patient " << i<< " a attendu " << tmp.getPatient()[i].getDureeHopital() << " pour un "<< e.getNom()<<std::endl;
                            double s = tmp.score();
                            //tmp.afficher();
                            //std::cout << "erreur 6" << std::endl;
                            //std::cout<< "Faisable à l'horaire "<< u << ", pour un score de  : "<< s<<std::endl; 
                            if (s<min_score){
                                min_score=s;
                                ubest=u;
                                std::cout<<"Nouveau meilleur horaire : "<< ubest << ", pour un score de  : "<< min_score<<std::endl;
                            } 
                            //std::cout << "erreur 7" << std::endl;
                            /*
                            for (int k = 0; k<e.getDuree(); k++){
                                horaire[u+k].retirerPatient(patient[i].getId());
                            }*/
                            //std::cout << "horaire = " << horaire[u].getHeure() << std::endl;
                            
                            //std::cout << " Nb de patient a l'horaire "<< u << " " << horaire[u].getNbpatient() <<std::endl; 
                        } 
                    }
                    std::cout << "coucou" << std::endl;
                    for (int k = 0; k<e.getDuree(); k++){
                        horaire[ubest+k].ajouterPatient(patient[i]);
                        horaire[ubest+k].ajouterEtape(e);
                    }
                    //std::cout<<"\tMeilleur horaire : "<< ubest << ", pour un score de  : "<< min_score<<std::endl;
                    std::cout << "Elle est planifié à l'horaire "<< ubest <<std::endl;
                    unew=ubest+e.getDuree();
                    e.setEtat(true);
                }
            }  
        } 
    } 
} 

void Calendrier::tempsSejour(){
    for (int i=0; i<nbPatient;i++){
        //std::cout<<"ALEDDDDDDDD"<< std::endl;
        int debut=0;
        int flag=0;
        for(int u=0;u<155;u++){
            for(int w=0; w<horaire[u].getNbPatient();w++){
                if(horaire[u].getPatient()[w].getId()==patient[i].getId()){
                    debut=u;
                    flag=1;
                } 
                if (flag==1){
                    break;
                } 
            } 
            if (flag==1){
                break;
            } 
        }
        int fin=0;
        flag=0;
        for(int u=154;u>-1;u--){
            for(int w=0; w<horaire[u].getNbPatient();w++){
                if(horaire[u].getPatient()[w].getId()==patient[i].getId()){
                    fin=u;
                    flag=1;
                } 
                if (flag==1){
                    break;
                } 
            } 
            if (flag==1){
                break;
            } 
        }  
        
        int duree = fin - debut + 1 ;//+ nb_nuit;
        // Conversion de la duree en h
        double tps = 0;
        int nombreHeure = 0;
        while (duree - 3 >= 0){
            duree -= 3;
            nombreHeure++;
        } 
        tps = nombreHeure + (double)(duree * 0.20);
        //std::cout << "Le patient " << i<< " a attenduuuuu " << tps << std::endl;
        patient[i].setDureehopital(tps); 
    } 
} 
void Calendrier::tempsSejour(int indice){
    int debut=0;
    int fin=155;
    int flagd=0;
    int flagf=0;
    int u=0;
    while(u<154/2){
        for(int w=0; w<horaire[u].getNbPatient();w++){
            if(horaire[u].getPatient()[w].getId()==patient[indice].getId()){
                debut=u;
                flagd=1;
            } 
            if (flagd==1){
                break;
            } 
        } 
        for(int w=0; w<horaire[154-u].getNbPatient();w++){
            if(horaire[154-u].getPatient()[w].getId()==patient[indice].getId()){
                fin=u;
                flagf=1;
            } 
            if (flagf==1){
                break;
            } 
        } 
        if(flagd!=1){
            u++;
        } 
        else{
            if(flagf!=1){
                u++;
            } 
            else{
                break;
            } 
        } 
    }  /*
    for(int u=0;u<155;u++){
        for(int w=0; w<horaire[u].getNbPatient();w++){
            if(horaire[u].getPatient()[w].getId()==patient[indice].getId()){
                debut=u;
                flag=1;
            } 
            if (flag==1){
                break;
            } 
        } 
        if (flag==1){
            break;
        } 
    }
    int fin=0;
    flag=0;
    for(int u=154;u>-1;u--){
        for(int w=0; w<horaire[u].getNbPatient();w++){
            if(horaire[u].getPatient()[w].getId()==patient[indice].getId()){
                fin=u;
                flag=1;
            } 
            if (flag==1){
                break;
            } 
        } 
        if (flag==1){
            break;
        } 
    }  */
     
    int duree = fin - debut + 1 ;//+ nb_nuit;
        // Conversion de la duree en h
    double tps = 0.0;//duree/3 + (duree%3)*0.2;
    int nombreHeure = 0;
    while (duree - 3 >= 0){
        duree -= 3;
        nombreHeure++;
    } 
    tps = (double)nombreHeure + (double)(duree * 0.20);
    std::cout << "Le patient " << indice<< " a attendu " << tps << " Nbheure : "<< nombreHeure<< " debut : "<< debut<< " fin : "<<fin<<std::endl;
    patient[indice].setDureehopital(tps); 
    std::cout << "Le patient " << indice<< " a attendu " << patient[indice].getDureeHopital()<< std::endl;
} 
double Calendrier::score(){
    double s = 0;
    for(int i=0; i<nbPatient; i++){
        double dur=patient[i].getDureeHopital();

        // On rajoute les nuits
        int deb = PremiereApparitionPatient(i+1);
        int fin = DerniereApparitionPatient(i+1);
        
        if (deb <= 30 && fin >=31 )
            dur += 14;
        else if (deb <= 61 && fin >= 62)
            dur += 14;
        else if (deb <= 92 && fin >= 93)
            dur += 14;
        else if (deb <= 123 && fin >= 124)
            dur += 14;

        if (dur == 0){
            s+= (1+106.2)*(1+106.2); //temps max que l'on peut attendre
        }
        else{
            s+=(1+dur)*(1+dur);
        } 
        //std::cout << "Duree du patient " << i <<" est de " << dur << " se qui donne une valeur de " << (1+dur)*(1+dur)<< " et ainsi un score de "<<s<< std::endl;
    }
    return s;
} 

int Calendrier::PremiereApparitionPatient(int id){
    int deb = 0;
    bool trouve = false;
    for (int i = 0; i < 155; i++){
        for (int j = 0; j < horaire[i].getNbPatient(); j++){ 
            if(horaire[i].getPatient()[j].getId() == id){
                deb = i;
                trouve = true;
                break;
            } 
        }
        if(trouve)
            break;
    }
    return deb;
}
int Calendrier::DerniereApparitionPatient(int id){
    int deb = 0;
    bool trouve = false;
    for (int i = 154; i > -1; i--){
        for (int j = 0; j < horaire[i].getNbPatient(); j++){ 
            if(horaire[i].getPatient()[j].getId() == id){
                deb = i;
                trouve = true;
                break;
            } 
        }
        if(trouve)
            break;
    }
    return deb;
}
double Calendrier::score(int nbp){
    double s = 0;
    for(int i=0; i<nbp; i++){
        double dur=patient[i].getDureeHopital(); 
        if (dur == 0){
            s+= (1+106.2)*(1+106.2); //temps max que l'on peut attendre
        }   
        else{
            s+=(1+dur)*(1+dur);
        } 
        //std::cout << "Duree du patient " << i <<" est de " << dur << " se qui donne une valeur de " << (1+dur)*(1+dur)<< " et ainsi un score de "<<s<< std::endl;
    } 
    return s;
} 

void Calendrier::remove(int id){
    for (int j=0;j<155;j++){
        //std::cout<<j<<std::endl;
        for (int k=0;k<horaire[j].getNbPatient();k++){
            if (horaire[j].getPatient()[k].getId()==id){
                horaire[j].retirerPatient(id);
            } 
        } 
    } 
} 

void Calendrier::Local_Search(int iterMax){

    Calendrier tmp(hopital);

    for(int i=0;i<nbPatient;i++){
        tmp.ajouter(patient[i]);
    } 
    tmp.RAZ();
    tmp.setHoraire(horaire);
    
    for (int i = 0; i < iterMax ; i++) 
    {
        for (int j=0;j<tmp.getNbPatient();j++){
            tmp.remove(tmp.getPatient()[j].getId());
        } 

        tmp.setHoraire(horaire);

        int indice = NbAleat(0,nbPatient); // indice du patient a deplacer
        //std::cout<<"Iteration "<<i<< " indice " << indice<<std::endl;
        //std::cout << "Patient atteint de "<<patient[indice].getMaladie().getNom()<<std::endl;
        double s = score(); // score actuel
        double new_s = (1+106.2)*(1+106.2)*nbPatient; // nouveau score
        
        // On retire tout les anciens rdv du patient a deplacer
        tmp.remove(patient[indice].getId());
        
        int it=0;
        while ( it<100){
            it++;
            int i_hor = NbAleat(0,154); // horraire aléatoire 
            
            int faisable=1;

            for(int j=0;j<patient[indice].getMaladie().getNbetape();j++ ){
                //std::cout << "Etape "<<j<<" : "<<patient[indice].getMaladie().getEtape()[j].getNom()<<std::endl; 
                for (int u=i_hor;u<155;u++){
                    //std::cout<<"Horaire "<<u<<std::endl;
                    faisable=1;
                    if (u+patient[indice].getMaladie().getEtape()[j].getDuree()>155){
                        //std::cout<<"Trop long, depasse 155 "<<std::endl;
                        faisable=0;
                    }
                    else{
                        //std::cout<<"PAS trop long, depasse 155 "<<std::endl;
                        if (tmp.getHoraire()[u+patient[indice].getMaladie().getEtape()[j].getDuree()].getHeure() > tmp.getHoraire()[u].getHeure()){
                            //std::cout<<"PAS trop long, dépasse la journée "<<std::endl;
                            for (int l=0; l<patient[indice].getMaladie().getEtape()[j].getNbressource(); l++){
                                for (int k=0; k<patient[indice].getMaladie().getEtape()[j].getDuree(); k++){
                                    if(tmp.getHoraire()[u+k].libre(patient[indice].getMaladie().getEtape()[j].getRessource()[l])==false){
                                        //std::cout<<"La ressource  "<<patient[indice].getMaladie().getEtape()[j].getRessource()[l].getNom()<< " est manquante"<<std::endl;
                                        faisable = 0; 
                                        break;
                                    } 
                                } 
                                if(faisable == 0){
                                    //std::cout<<"Il manque une ressource"<<std::endl;
                                    break;
                                } 
                            }
                            if(faisable==1){
                                //std::cout<<"Il y a toutes les ressources necéssaire a l'horaire "<<u<<std::endl;
                            } 
                        }
                        else{
                            //std::cout<<"Trop long, dépasse la journée "<<std::endl;
                            faisable=0;
                        }  
                    } 
                    if(faisable == 1){
                        //std::cout<<"On choisit l'horaire "<<u<<std::endl;

                        i_hor=u+patient[indice].getMaladie().getEtape()[j].getDuree();
                       
                        for (int k = 0; k<patient[indice].getMaladie().getEtape()[j].getDuree(); k++){
                            tmp.getHoraire()[u+k].ajouterPatient(patient[indice]);
                            tmp.getHoraire()[u+k].ajouterEtape(patient[indice].getMaladie().getEtape()[j]);
                        } 
                        break;
                    } 
                } 
                if(faisable==0){
                    //std::cout<<"Une etape n'a pas plus etre placee, on change d'horaire de départ "<<std::endl;
                    tmp.remove(patient[indice].getId());
                    break;
                } 
            } 
            if (faisable==1){
                tmp.tempsSejour();
                new_s = tmp.score();
                if(s<=new_s){
                    tmp.remove(patient[indice].getId());
                } 
                else{
                    std::cout<<"Nouveau score "<<s<<"=>"<<new_s<<std::endl;
                    break;
                } 
            } 
        } 
        if (it<100){        
            double eeee=tmp.score();

            for (int j=0;j<tmp.getNbPatient();j++){
                remove(tmp.getPatient()[j].getId());
            }             
            setHoraire(tmp.getHoraire());
            
            tempsSejour();
            double ds=score();
        } 
    }
}

void Calendrier::DeplacerPatient(int IdPatient, int numEtape, int oldHoraire, int newHoraire){
    std::cout << "FONCTION DEPLACERPATIENT" << std::endl;
    int IdPatientHoraire = 0;
    for (int i = 0; i < horaire[oldHoraire].getNbPatient() ; i++)// Pour récupérer la position de notre patient dans horaire
    {
        if(horaire[oldHoraire].getPatient()[i].getId() == IdPatient){ 
            IdPatientHoraire = i;
        } 
    }  

    int dureeEtape = horaire[oldHoraire].getPatient()[IdPatientHoraire].getMaladie().getEtape()[numEtape].getDuree();
    std::cout << "Duree etape :" << dureeEtape << " du patient " << IdPatient << std::endl;
    bool ressourceDispo = true;
    
    for (int i = 0; i < dureeEtape ; i++){ // On vérifie que les ressources sont dispo tout le long de l'étape
        int nbRessource = horaire[newHoraire + i].getNbressource();
        int ressourceUtilise = 0;
        for (int b=0;b<nbRessource; b++){
            if (horaire[newHoraire + i].getRessource()[b].getEtat()==false){
                ressourceUtilise ++;
            }
        }
        if (nbRessource == ressourceUtilise)
            ressourceDispo = false;
    }

    if (!ressourceDispo)
        std::cout << "Le patient " << horaire[oldHoraire].getPatient()[IdPatientHoraire].getId() << "Ne peut pas être déplacer à l'horaire " << newHoraire << std::endl; 

    if (ressourceDispo){
        //On libere les ressources à l'ancien horaire ancien que le patient
        for (int i=0; i < dureeEtape; i++){
            bool cond = true;
            for (int b=0;b<horaire[oldHoraire].getNbressource(); b++){ 
                if ((horaire[oldHoraire + i].getRessource()[b].getEtat()==false) && (cond)){
                    horaire[oldHoraire + i].getRessource()[b].setEtat(true);
                    cond = false;
                }
            }
            horaire[oldHoraire + i].retirerPatient(IdPatient);
        }

        //On bloque les ressource au new horaire
        for (int i=0; i < dureeEtape; i++){
            bool cond = true;
            for (int b=0;b<horaire[newHoraire].getNbressource(); b++){ 
                if ((horaire[newHoraire + i].getRessource()[b].getEtat()==true) && (cond)){
                    horaire[newHoraire + i].getRessource()[b].setEtat(false);
                    cond = false;
                } 
            }  
        }

        //On ajoute le patient sur le nouveau créneau
        Etape e = patient[IdPatient].getMaladie().getEtape()[numEtape];
        for (int k = 0; k<dureeEtape; k++){
            horaire[newHoraire+k].ajouterPatient(patient[IdPatientHoraire]);
            horaire[newHoraire+k].ajouterEtape(e);
        }
    } 
}  

void Calendrier::RAZ(){

    for (int i=0;i<155;i++){
        //std::cout<<"Bonjouuuur "<<i<<std::endl;
        for (int j=0;j<horaire[i].getNbPatient();j++){
            horaire[i].retirerPatient(horaire[i].getPatient()[j].getId());
            //std::cout<<"ici? "<<std::endl;
        }  
        //horaire[i].setNbPatient(0);
        /*for (int j=0;j<horaire[i].getNbetape();j++){
            horaire[i].retirerEtape(0);
            //std::cout<<"ici? "<<std::endl;
        }  */
        
    } 
    for(int j=0;j<nbPatient;j++){
        for (int l=0;l<patient[j].getMaladie().getNbetape();l++){
            patient[j].getMaladie().getEtape()[l].setEtat(false);  
        } 
    } 
}  
void Calendrier::RAZ_patient(){
    RAZ();
    patient = new Patient[100]; 
} 

void Calendrier::regrets(int k){
    int*** tabPatient = new int**[nbPatient];//Pour garder en mémoire la position des étapes 
    for (int i = 0; i < nbPatient; ++i) {
        tabPatient[i] = new int*[k];
        for (int j = 0; j < k; ++j) {
            tabPatient[i][j] = new int[4];// 4 etapes max
        }
    }

    for (int i = 0; i < nbPatient; ++i) {
        for (int j = 0; j < k; ++j) {
            for (int g = 0; g < 4; g++){
                tabPatient[i][j][g] = -2;// 4 etapes max
            } 
        }
    }

    for (int i = 0; i < nbPatient; i++) {
        for (int j = 0; j < k; j++) {
            for (int l = 0; l<4;l++){
                tabPatient[i][j][l] = -1;
            } 
        }
    }

    int boolPatient[nbPatient];
    for (int i=0;i<nbPatient;i++){ 
        boolPatient[i] = 0;
    }
    
    double* tempsPatient[nbPatient];
    for (int i=0; i < nbPatient; i++)
        tempsPatient[i] = new double[k];  


    int nbPatientPlace = 0; //prend en compte ceux placés dans le planning et ceux impossible à placer
    int patientPasplace[nbPatient];
    int indicePatientPasPlace = 0;

    double* scorePatient[nbPatient];
    for (int i = 0; i < nbPatient; i++)
        scorePatient[i] = new double[2];// premiere case le scoredu max - min et deuxime case la position du min  
        
     

    while ( nbPatientPlace < nbPatient) {
        bool probleme = false;
        //std::cout << "Nb patient positionné " << nbPatientPlace << std::endl;
        for (int i=0; i < nbPatient; i++){
            probleme = false;
            if (!boolPatient[i]) { 
                for (int r = 0; r < k; r++){ 

                    std::string maladie = patient[i].getMaladie().getNom();
                    int pos=0;
                    int& position = pos;
                    int iteration = 0;

                    while (iteration < 100 && !probleme){
                        if(maladie == "Fracture") { //FRACTURE                      
                            if (InsertionPatientPossible(patient[i].getId(),0,0,144,pos)){
                                //std::cout << "i = " << i << "id patient " << patient[i].getId() << std::endl;
                                tabPatient[i][r][0] = position;
                            } 
                            else
                                probleme = true;  
                            
                            if (!probleme && InsertionPatientPossible(patient[i].getId(),1,tabPatient[i][r][0]+3,144+3,pos))
                                tabPatient[i][r][1] = position;
                            else
                                probleme = true;  
                            
                            if (!probleme && InsertionPatientPossible(patient[i].getId(),2,tabPatient[i][r][1]+6,144+3+6,pos))
                                tabPatient[i][r][2] = position;
                            else
                                probleme = true;                           
                        } 
                        else if(maladie == "Covid"){ //COVID
                            if (InsertionPatientPossible(patient[i].getId(),0,0,152,pos))
                                tabPatient[i][r][0] = position;
                            else
                                probleme = true;
                        } 
                        else if (maladie == "Cancer") {//CANCER
                            if (InsertionPatientPossible(patient[i].getId(),0,0,130,pos))
                                tabPatient[i][r][0] = position;
                            else
                                probleme = true;  
                            
                            if (!probleme && InsertionPatientPossible(patient[i].getId(),1,tabPatient[i][r][0]+6,130+6,pos))
                                tabPatient[i][r][1] = position;
                            else
                                probleme = true;  
                            
                            if (!probleme && InsertionPatientPossible(patient[i].getId(),2,tabPatient[i][r][1]+6,130+6+6,pos))
                                tabPatient[i][r][2] = position;
                            else
                                probleme = true;
                            if (!probleme && InsertionPatientPossible(patient[i].getId(),3,tabPatient[i][r][2]+6,130+6+6+6,pos))
                                tabPatient[i][r][3] = position;
                            else
                                probleme = true;
                        }
                        else if (maladie == "Accouchement") {
                            if (InsertionPatientPossible(patient[i].getId(),0,0,145,pos))
                                tabPatient[i][r][0] = position;
                            else
                                probleme = true;  
                            
                            if (!probleme && InsertionPatientPossible(patient[i].getId(),1,tabPatient[i][r][0]+1,145+1,pos))
                                tabPatient[i][r][1] = position;
                            else
                                probleme = true;          
                        }
                        iteration++;
                        if (probleme == true)
                            probleme = false;
                        else
                            break;

                    }
                    if (iteration == 100){
                        boolPatient[i] = 1;
                        patientPasplace[indicePatientPasPlace] = i;
                        indicePatientPasPlace++;
                        nbPatientPlace ++;
                    } 
                } 
            } 
        }

        //calcul du score à chaque k etape
        for (int i=0; i < nbPatient; i++){// boucle normalement OK
            if (!boolPatient[i]){ 
                //std::cout << "I = " << i << std::endl;
                for (int l=0; l<k; l++){ 
                    tempsPatient[i][l] = scoreUnPatient(i,tabPatient[i][l]);
                     
                }/*
                if(patient[i].getMaladie().getNom() == "Covid"){
                    for (int r = 0; r<k;r++)
                        std::cout << "Patient " << i + 1 << "horaire vaccin " << tabPatient[i][r][0] << std::endl;  
                    std::cout << std::endl;
                } */
            } 
        }
        
        //std::cout << "ICI 0" << std::endl;
        //determiner le score du max - min, et mettre l'indice du min de chaque patient 
        for (int i = 0; i < nbPatient; i++){ 
            if (!boolPatient[i]){ 
                    scorePatient[i][0] = maxListe(tempsPatient[i], k) - minListe(tempsPatient[i], k);
                    
                    int indiceScoreMin = 0;
                    while (tempsPatient[i][indiceScoreMin] != scoreUnPatient(i,tabPatient[i][indiceScoreMin]))
                        indiceScoreMin++;
                    scorePatient[i][1] = indiceScoreMin; 
            } 
        }
        //std::cout << "Affichage score " << tempsPatient[i][l] << " pour patient " << i << std::endl;
        //std::cout << "ICI 1" << std::endl;

        //trouver le patient avec le score max
        double scoreMax = -1;
        int indiceScoreMax = 0;

        for (int i = 0; i < nbPatient; i++){
            if (!boolPatient[i]){
                if (scorePatient[i][0] > scoreMax){
                    scoreMax = scorePatient[i][0];
                    indiceScoreMax = i; 
                } 
            } 
        } 
        //std::cout << "ICI 2" << std::endl;

         
        //POSITIONNER LE PATIENT
        if (!boolPatient[indiceScoreMax]){ 
            for (int i = 0; i < patient[indiceScoreMax].getMaladie().getNbetape(); i++){
                if(patient[indiceScoreMax].getMaladie().getNom() == "Cancer")
                    std::cout << "Place a l etape " << nbPatientPlace << " etape " << i << " duree etape " << patient[indiceScoreMax].getMaladie().getEtape()[i].getDuree() << std::endl;
                for (int g = 0; g<patient[indiceScoreMax].getMaladie().getEtape()[i].getDuree(); g++){
                    //std::cout << "Indice score max" << indiceScoreMax << std::endl;
                    //std:: cout << "Position min int " << (int)scorePatient[indiceScoreMax][1] << " double " << scorePatient[indiceScoreMax][1] << std::endl;
                    //std::cout << "Horaire " << tabPatient[indiceScoreMax][(int)scorePatient[indiceScoreMax][1]][i] + g << std::endl;
                    if(patient[indiceScoreMax].getMaladie().getNom() == "Cancer")
                        std::cout << g << std::endl;
                    horaire[ tabPatient[indiceScoreMax][(int)scorePatient[indiceScoreMax][1]][i] + g].ajouterPatient(patient[indiceScoreMax]);
                    horaire[ tabPatient[indiceScoreMax][(int)scorePatient[indiceScoreMax][1]][i] + g].ajouterEtape(patient[indiceScoreMax].getMaladie().getEtape()[i]);
                }
            }
        }
        //std::cout << "ICI 3" << std::endl;
        boolPatient[indiceScoreMax] = 1; 
        nbPatientPlace++;
    }

    for (int i = 0; i < indicePatientPasPlace; i++){
        std::cout << "Impossible de placer le patient " << patientPasplace[i] << std::endl;
    } 
}


int Calendrier::NbAleat(int deb, int fin){
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(deb, fin);
    int random_num = dis(gen);
    return random_num;
} 

bool Calendrier::InsertionPatientPossible(int IdPatient, int etape, int deb, int fin, int& position){ //On vérifie juste pour une étape
    IdPatient = IdPatient - 1;
    position = NbAleat(deb, fin);
    bool faisable = false;
    int iter = 0;
    int h = 0;
    while (!faisable && iter < 150){ 
        faisable = true;
        h = NbAleat(deb, fin);
        //std::cout << "Heure : " << h << std::endl;
        //std::cout << "Etape" << etape << " duree " << patient[IdPatient].getMaladie().getEtape()[etape].getDuree() << std::endl;
        //std::cout << "h0 : " << horaire[h+patient[IdPatient].getMaladie().getEtape()[etape].getDuree()].getHeure() << " h1 : " << horaire[h].getHeure() << std::endl;
        if (horaire[h+patient[IdPatient].getMaladie().getEtape()[etape].getDuree()].getHeure() >= horaire[h].getHeure()){ 
            //std::cout << "ok pour h" << std::endl;
            for (int l=0; l<patient[IdPatient].getMaladie().getEtape()[etape].getNbressource(); l++){
                for (int k=0; k<patient[IdPatient].getMaladie().getEtape()[etape].getDuree(); k++){
                    if(horaire[h+k].libre(patient[IdPatient].getMaladie().getEtape()[etape].getRessource()[l])==false){
                        faisable = false;
                        break;
                    } 
                } 
            }
        }
        else{ 
            //std::cout << "PAS ok pour heure" << std::endl;
            faisable = false; 
        } 
        iter++;
    }
    position = h;

    //std::cout << "Position " << position << "Etape" << etape << " duree " << patient[IdPatient].getMaladie().getEtape()[etape].getDuree() << " h1 : " << horaire[h].getHeure() <<std::endl;
    return faisable;
} 

double Calendrier::scoreUnPatient(int IdPatient, int date[]){
    int nbEtape = patient[IdPatient].getMaladie().getNbetape();
    int deb = date[0];
    int fin = date[nbEtape - 1];

    std::string maladie = patient[IdPatient].getMaladie().getNom();
    if (maladie == "Fracture")
        fin += 0;
    else if (maladie == "Covid")
        fin += 1;
    else if (maladie == "Cancer")
        fin += 5;
    else if (maladie == "Accouchement")
        fin += 7;

    int duree = fin - deb + 1 ;//+ nb_nuit;
    // Conversion de la duree en h
    double tps = 0;
    int nombreHeure = 0;

    //On prend en compte les nuits passées à l'hopital
    if (deb <= 30 && fin >=31 )
        nombreHeure += 14;
    else if (deb <= 61 && fin >= 62)
        nombreHeure += 14;
    else if (deb <= 92 && fin >= 93)
        nombreHeure += 14;
    else if (deb <= 123 && fin >= 124)
        nombreHeure += 14;

    while (duree - 3 >= 0){
        duree -= 3;
        nombreHeure++;
    } 
    tps = nombreHeure + (double)(duree * 0.20);

    if (tps == 0)
        tps = 100;
    double score = (tps + 1)*(tps + 1);
    return score;
} 

double Calendrier::maxListe(int tab[]){
    int size = 0;
    while (tab[size] >= 0)
        size ++;

    int maxi = 0;
    for (int i = 0; i < size; i++){
        if (tab[i] > maxi)
            maxi = tab[i]; 
    }
    return maxi;
}
double Calendrier::maxListe(double tab[], int k){
    /*
    int size = 0;
    while (tab[size] >= 0){ 
        size ++;
        std::cout << "size = " << size << std::endl;
    } 
    std::cout << "size = " << size << std::endl;*/

    int maxi = 0;
    for (int i = 0; i < k; i++){
        if (tab[i] > maxi)
            maxi = tab[i]; 
    }
    return maxi;
}

double Calendrier::minListe(int tab[]){
    int size = 0;
    while (tab[size] >= 0)
        size ++;

    double mini = 1000;
    for (int i = 0; i < size; i++){
        if (tab[i] < mini)
            mini = tab[i]; 
    }
    return mini;
}
double Calendrier::minListe(double tab[], int k){
    /*int size = 0;
    while (tab[size] >= 0)
        size ++;*/

    double mini = 100000;
    for (int i = 0; i < k; i++){
        if (tab[i] < mini)
            mini = tab[i]; 
    }
    return mini;
}
/*Calendrier::~Calendrier() {
    delete[] patient;
    delete[] horaire;
}*/