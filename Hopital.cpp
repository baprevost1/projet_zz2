#include "Hopital.hpp"

Hopital::Hopital(const char* name) : nom(name),nbressource(0),nbDocteurPartiel(0),nbDocteur(0),nbMateriel(0){
    ressource = new Ressource[100];
} 
Hopital::Hopital() : nom(""),nbressource(0),nbDocteurPartiel(0),nbDocteur(0),nbMateriel(0){
    ressource = new Ressource[100];
} 
Ressource* Hopital::getRessource(){
    return ressource;
} 
void Hopital::ajouter(Ressource r){
    ressource[nbressource].setNom(r.getNom());
    ressource[nbressource].setEtat(r.getEtat());
    if(r.getNom() == "Docteur"){
        nbDocteur++;
    } 
    if(r.getNom() == "Materiel"){
        nbMateriel++;
    } 
    nbressource++;
} 
void Hopital::ajouterPartiel(Ressource r){
    ressource[nbressource].setNom(r.getNom());
    ressource[nbressource].setEtat(r.getEtat());
    nbDocteurPartiel++;
    nbressource++;
} 
std::string Hopital::getNom(){
    return nom;
}
int Hopital::getNbressource(){
    return nbressource;
} 
int Hopital::getNbdocteur(){
    return nbDocteur;
} 
int Hopital::getNbmateriel(){
    return nbMateriel;
}  
void Hopital::afficher(){
    for(int i=0;i<nbressource;i++){
        std::cout << ressource[i].getNom() << std::endl; 
    } 
}
/*Hopital::~Hopital(){
    delete[] ressource;
}*/