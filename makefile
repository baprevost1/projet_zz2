# Nom du compilateur
CXX = g++

# Options de compilation
CXXFLAGS += -Wall -Wextra 

# Nom de l'exécutable final
EXE = main

# Liste des fichiers source
SRC = main.cpp calendrier.cpp Etape.cpp Hopital.cpp Maladie.cpp Patient.cpp Ressource.cpp Horaire.cpp Population.cpp

OBJ=$(addprefix build/,$(SRC:.cpp=.o))
DEP=$(addprefix build/,$(SRC:.cpp=.d))

all: $(OBJ)
	$(CXX) -o $(EXE) $^ $(LDFLAGS)

build/%.o: %.cpp
	@mkdir -p build
	$(CXX) $(CXXFLAGS) -o $@ -c $<

clean:
	rm -rf build core *.gch

-include $(DEP)
